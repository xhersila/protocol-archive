create table user(
userid int primary key varchar(50),
name varchar(50),
prename varchar(50),
password int);

create table team (
	team_id int primary key,
	name varchar(25)
);

create table user_team (
	userid varchar(50),
	team_id int,
	primary key(userid, team_id),
	foreign key(userid) references user(userid),
	foreign key(team_id) references team(team_id)
);

create table goals (
goal_id int primary key,
goal_description text,
goal_type varchar(10)
);

create table user_goals (
	userid varchar(50),
	goal_id int,
	primary key(goal_id, userid),
	foreign key(userid) references user(userid),
	foreign key(goal_id) references goals(goal_id)
);

CREATE table protocol (
protocol_id int primary key,
recorder_id varchar(50),
moderator_id varchar(50),
general_news text,
general_announcements text,
general_requests text,
location varchar(15),
start_time varchar(5),
end_time varchar(5),
protocol_week int,
protocol_year int,
foreign key(recorder_id) references user(userid),
foreign key(moderator_id) references user(userid)
);

Create table protocol_goals (
	protocol_id int,
	goal_id int,
	primary key(goal_id, protocol_id),
	foreign key(protocol_id) references protocol (protocol_id),
	foreign key(goal_id) references goals(goal_id)
);

create table protocol_participant (
	userid varchar(50),
	protocol_id varchar(10),
	primary key(userid, protocol_id),
	foreign key(userid) references user(userid),
	foreign key(protocol_id) references protocol(protocol_id)
);

create table protocol_absentee (
	userid varchar(50),
	protocol_id varchar(10),
	primary key(userid, protocol_id),
	foreign key(userid) references user(userid),
	foreign key(protocol_id) references  protocol(protocol_id)
);
