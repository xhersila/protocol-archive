const express = require("express");
const router = express.Router();

const sqlGoals = require("../server_side/sqlGoals");

router.put("/main/goal/team", async function (req, res, next) {
  await sqlGoals.addTeamGoal(
    req.body.goal_description,
    req.body.userid,
    req.body.team_id,
    req.body.protocol_id
  );
  res.send("DONE");
});

router.get("/main/goal/team/:protocol_id", async function (req, res, next) {
  const response = await sqlGoals.getTeamGoal(req.params.protocol_id);
  res.send(response);
});

router.put("/main/goal/individual", async function (req, res, next) {
  await sqlGoals.addIndividualGoal(
    req.body.goal_description,
    req.body.userid,
    req.body.team_id,
    req.body.protocol_id
  );
  res.send("DONE");
});

router.get("/main/goal/individual/:protocol_id", async function (req, res, next) {
  const response = await sqlGoals.getIndividualGoal(
    req.params.protocol_id
  );
  res.send(response);
});

router.post("/main/goal/:goal_id", async function (req, res, next) {
  await sqlGoals.editGoal(req.body.goal_id, req.body.goal_description);
  res.send("DONE");
});

router.put("/main/delete/", async function (req, res, next) {
  await sqlGoals.deleteGoal(req.body.goal_id);
  res.send("DONE");
});

module.exports = router;
