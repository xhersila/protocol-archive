const express = require("express");
const router = express.Router();

const sqlEmployees = require("../server_side/sqlEmployees")

router.post("/main/selectedParticipants/", async function (req, res, next) {
  participants = req.body.selectedParticipant;
  participants_name = req.body.selectedParticipantName;

  for (var i = 0; i < req.body.selectedParticipant.length; i++) {
    sqlEmployees.addParticipants(
      participants[i],
      req.body.protocol_id,
      participants_name[i]
    );
  }

  res.send("DONE");
});

router.post("/main/selectedAbsentees/", async function (req, res, next) {
  absentees = req.body.selectedAbsentee;
  absentees_name = req.body.selectedAbsenteeName;

  for (var i = 0; i < req.body.selectedAbsentee.length; i++) {
    sqlEmployees.addAbsentees(
      absentees[i],
      req.body.protocol_id,
      absentees_name[i]
    );
  }

  res.send("DONE");
});

router.get("/main/participants/:protocol_id", async function (req, res, next) {
  const response = await sqlEmployees.getParticipants(
    req.params.protocol_id
  );
  res.send(response);
});

router.get("/main/absentees/:protocol_id", async function (req, res, next) {
  const response = await sqlEmployees.getAbsentees(req.params.protocol_id);
  res.send(response);
});

router.get("/main/moderator/:protocol_id", async function (req, res, next) {
  const response = await sqlEmployees.getModerator(req.params.protocol_id);
  res.send(response);
});

router.get("/main/recorder/:protocol_id", async function (req, res, next) {
  const response = await sqlEmployees.getRecorder(req.params.protocol_id);
  res.send(response);
});

router.put("/main/deleteParticipants/:protocol_id", async function (req, res, next) {
  const response = await sqlEmployees.deleteParticipants(req.params.protocol_id);
  res.send(response);
});

router.put("/main/deleteAbsentees/:protocol_id", async function (req, res, next) {
  const response = await sqlEmployees.deleteAbsentees(req.params.protocol_id);
  res.send(response);
});

module.exports = router;
