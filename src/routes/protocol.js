const express = require("express");
const router = express.Router();

const sqlProtocol = require("../server_side/sqlProtocol.js");

router.put("/main/protocol/", async function (req, res, next) {
  await sqlProtocol.addProtocol(
    req.body.protocol_start,
    req.body.protocol_end,
    req.body.protocol_id,
    req.body.userid,
    req.body.goal_id
  );
  res.send("DONE");
});

router.post("/main/protocol/", async function (req, res, next) {
  await sqlProtocol.editInformationSection(
    req.body.protocol_id,
    req.body.date,
    req.body.location,
    req.body.start_time,
    req.body.end_time,
    req.body.recorder_id,
    req.body.moderator_id
  );
  res.send("DONE");
});

router.post("/main/general/", async function (req, res, next) {
  await sqlProtocol.editGeneralSection(
    req.body.protocol_id,
    req.body.general_news,
    req.body.general_requests,
    req.body.general_announcements
  );
  res.send("DONE");
});

router.get("/main/general/:protocol_id", async function (req, res, next) {
  const response = await sqlProtocol.getGeneralInformation(
    req.params.protocol_id
  );
  res.send(response);
});

router.get("/main/information/:protocol_id", async function (req, res, next) {
  const response = await sqlProtocol.getInformationSection(
    req.params.protocol_id
  );
  res.send(response);
});


module.exports = router;
