const express = require("express");
const router = express.Router();

const sqlGeneral = require("../server_side/sqlGeneral");

router.get("/main/:userid/", function (req, res, next) {
  res.render("index");
});

router.get("/login/:username/:password/", async function (req, res, next) {
  const response = await sqlGeneral.getUsername(
    req.params.username,
    req.params.password
  );
  const valid = checkCredentials(response);
  res.send(valid);
});

router.get("/userinfo/:userid/", async function (req, res, next) {
  const response = await sqlGeneral.getUserinfo(req.params.userid);
  res.send(response);
});

router.get("/main/team/:userid/", async function (req, res, next) {
  const response = await sqlGeneral.getTeam(req.params.userid);
  res.send(response);
});


function checkCredentials(usernameDBResponse) {
  if (usernameDBResponse[0] != undefined) {
    return "valid";
  }
  return "invalid";
}



module.exports = router;