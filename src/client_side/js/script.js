

var userid;
let protocol_id;
var offset = 0;
var today = new Date();
var dayOfWeekOffset = today.getDay();
var url = window.location.href;
index = url.length;
userid = url.slice(54, index-1);
var team_id;
var goal_id;
var team_one;
var team_two;
const server = "https://protocolarchive.azurewebsites.net/";
//const server = "http://localhost:8080/";


window.onload = function () {
  getName();
  getInitialContent();
  getWeek();
  getTeam();
  getGeneralInfo();
  getIntroSection();
  getTeamGoal();
  getInitialIndividualGoals();
};

//Download the contents
function download() {

  const wholeContent = document.getElementById("wholeContent");
  var opt = {
    margin: 0.2,
    padding: 1,
    filename: protocol_begin + "-" + protocol_end,
    image: { type: "jpeg", quality: 1 },
    html2canvas: { scale: 1 },
    jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
    pagebreak: {before: '.newPage'},
  };
  

  html2pdf().set(opt).from(wholeContent).save();

}

//Get the introdcution section of the web app
async function getIntroSection() {
  const url =
    server + "main/information/" + protocol_id + "/";

  let intro;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",

    },
  });
  await fetch(request).then(async (res) => {
    intro = await res.json();
  });

  getParticipants();
  getAbsentees();
  getModerator();
  getRecorder();
  document.getElementById("displayLocation").innerHTML = intro[0].location;
  document.getElementById("displayStartTime").innerHTML = intro[0].start_time;
  document.getElementById("displayEndTime").innerHTML = intro[0].end_time;
  document.getElementById("displayDate").innerHTML = intro[0].date;
}

//Get employees present in the meeting
async function getParticipants() {
  const url =
    server + "main/participants/" + protocol_id + "/";

  let participants;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    participants = await res.json();
  });

  var result = [];

  for (var i in participants) result.push([participants[i]]);

  var displayParticipant = [];
  for (var i = 0; i < result.length; i++) {
    displayParticipant.push(" " + participants[i].participant_name);
  }

  document.getElementById("displayParticipants").innerHTML = displayParticipant;
}

//Get employees absent in the meeting
async function getAbsentees() {
  const url = server + "main/absentees/" + protocol_id + "/";

  let absentees;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    absentees = await res.json();
  });

  var result = [];

  for (var i in absentees) result.push([absentees[i]]);

  var displayAbsentee = [];
  for (var i = 0; i < result.length; i++) {
    displayAbsentee.push("    " + absentees[i].absentee_name);
  }
  document.getElementById("displayAbsentees").innerHTML = displayAbsentee;
}

//Get the recorder of the meeting
async function getRecorder() {
  const url = server + "main/recorder/" + protocol_id + "/";

  let recorder;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    recorder = await res.json();
  });

  document.getElementById("displayRecorder").innerHTML =
    recorder[0].recorder_id;
}


//Get the moderator of the meeting
async function getModerator() {
  const url = server + "main/moderator/" + protocol_id + "/";

  let moderator;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    moderator = await res.json();
  });

  document.getElementById("displayModerator").innerHTML =
    moderator[0].moderator_id;
}

async function deleteParticipants() {
  const url = server+ "main/deleteParticipants/" + protocol_id + "/";
  
  let participant;

  var request = new Request(url, {
    method: "PUT",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    participant = await res.json();
  });

  getParticipants();
}

async function deleteAbsentees() {
  const url = server + "main/deleteAbsentees/" + protocol_id + "/";

  let absentee;

  var request = new Request(url, {
    method: "PUT",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    absentee = await res.json();
  });

  getAbsentees();
}

//Get the user's name
async function getName() {
  const url = server + "userinfo/" + userid + "/";
  let response;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    response = await res.json();
  });

  document.getElementById("welcome").innerHTML =
    "Welcome To Raevoluz Protocol Archive, " + response[0].prename + "!";

  prename = response[0].prename;
  user_name = response[0].name;

  console.log(userid);
}

//Log in to the web app
async function login() {
  const username = document.getElementById("username").value;
  const password = document.getElementById("password").value;

  if (username == "" || password == "") {
    alert("Please enter username and password");
    return;
  }

  const url =
    server + "login/" + username + "/" + password + "/";
  let response;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    response = await res.text();
  });
  if (response == "valid") {
    userid = username;
    window.location.replace( server + "main/userid=" + userid + "/");
  } else {
    alert("Wrong Username or Password");
  }
}

//Get date range
async function getWeek(offset) {
  offset = offset || 0;
  var firstDay = new Date();
  firstDay.setDate(firstDay.getDate() - dayOfWeekOffset + offset * 7 + 1);
  var lastDay = new Date(firstDay);
  lastDay.setDate(lastDay.getDate() + 4);
  protocol_begin = makeDateString(firstDay);
  protocol_end = makeDateString(lastDay);
  range = makeDateString(firstDay) + " - " + makeDateString(lastDay);
  document.getElementById("date_range").innerHTML = range;
}

//Convert date to string
function makeDateString(date) {
  var dd = date.getDate();
  var mm = date.getMonth() + 1;
  var y = date.getFullYear();

  var dateString = dd + "/" + mm + "/" + y;
  protocol_id = "" + dd + mm + y;

  return dateString;
}

//Make backward arrow functional
function backward() {
  offset = offset - 1;
  getWeek(offset);
  addProtocol(protocol_begin, protocol_end, protocol_id);
  getIntroSection();
  getGeneralInfo();
  getTeamGoal();
  getInitialIndividualGoals();
}

//Make forward arrow functional
function forward() {
  offset = offset + 1;
  getWeek(offset);
  addProtocol(protocol_begin, protocol_end, protocol_id);
  getIntroSection();
  getGeneralInfo();
  getTeamGoal();
  getInitialIndividualGoals();
}

//Create new protocol
async function addProtocol(protocol_begin, protocol_end, protocol_id) {
  const url = server + "main/" + "protocol" + "/";

  let data = {
    protocol_id: protocol_id,
    protocol_start: protocol_begin,
    protocol_end: protocol_end,
    userid: userid,
  };
  var request = new Request(url, {
    method: "PUT",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (response) => {
    await response.text();
  });

  getGeneralInfo();
}

//Update the information section of the web app
async function updateInformationSection() {
  const url = server + "main/" + "protocol" + "/";

  var date = document.getElementById("protocol_date").value;
  var location = document.getElementById("location").value;
  var start_time = document.getElementById("start_time").value;
  var end_time = document.getElementById("end_time").value;

  var moderator = document.getElementById("moderator");
  var selectedModerator = moderator.options[moderator.selectedIndex].value;

  var recorder = document.getElementById("recorder");
  var selectedRecorder = recorder.options[recorder.selectedIndex].value;

  let data = {
    protocol_id: protocol_id,
    date: date,
    location: location,
    start_time: start_time,
    end_time: end_time,
    recorder_id: selectedRecorder,
    moderator_id: selectedModerator,
  };
  var request = new Request(url, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  await fetch(request).then(async (response) => {
    await response.text();
  });

  protocolParticipants();
  protocolAbsentees();

  document.getElementById("informationSection").style.display = "none";
  document.getElementById("informationSectionEdit").style.display =
    "inline-block";
  document.getElementById("participantss").style.display = "none";
  document.getElementById("absentt").style.display = "none";
  document.getElementById("moderatorr").style.display = "none";
  document.getElementById("recorderr").style.display = "none";
  document.getElementById("protocol_date").style.display = "none";
  document.getElementById("location").style.display = "none";
  document.getElementById("start_time").style.display = "none";
  document.getElementById("end_time").style.display = "none";
  document.getElementById("displayLocation").style.display = "block";
  document.getElementById("displayStartTime").style.display = "block";
  document.getElementById("displayEndTime").style.display = "block";
  document.getElementById("displayDate").style.display = "block";
  document.getElementById("displayModerator").style.display = "block";
  document.getElementById("displayRecorder").style.display = "block";
  document.getElementById("displayParticipants").style.display = "block";
  document.getElementById("displayAbsentees").style.display = "block";

  getIntroSection();
}

//Insert protocol Participants in the database
async function protocolParticipants() {
  var selectedParticipant = [];
  var selectedParticipantName = [];
  for (var option of document.getElementById("participants").options) {
    if (option.selected) {
      selectedParticipant.push(option.value);
      selectedParticipantName.push(option.innerHTML);
    }
  }

  const url = server + "main/" + "selectedParticipants" + "/";

  let data = {
    selectedParticipant: selectedParticipant,
    selectedParticipantName: selectedParticipantName,
    protocol_id: protocol_id,
  };

  var request = new Request(url, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  await fetch(request).then(async (response) => {
    await response.text();
  });
}

//Insert protocol absentees in the database
async function protocolAbsentees() {
  var selectedAbsentee = [];
  var selectedAbsenteeName = [];
  for (var option of document.getElementById("absent").options) {
    if (option.selected) {
      selectedAbsentee.push(option.value);
      selectedAbsenteeName.push(option.innerHTML);
    }
  }

  const url = server + "main/" + "selectedAbsentees" + "/";

  let data = {
    selectedAbsenteeName: selectedAbsenteeName,
    selectedAbsentee: selectedAbsentee,
    protocol_id: protocol_id,
  };
  var request = new Request(url, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (response) => {
    await response.text();
  });
}

//Update the general section of the web app
async function updateGeneralSection() {
  var general_news = quill3.root.innerHTML;
  var general_requests = quill4.root.innerHTML;
  var general_announcements = quill5.root.innerHTML;

  const url = server + "main/" + "general" + "/";
  let data = {
    protocol_id: protocol_id,
    general_news: general_news,
    general_requests: general_requests,
    general_announcements: general_announcements,
  };
  var request = new Request(url, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });

  quill3.disable();
  quill4.disable();
  quill5.disable();
  document.getElementById("editGeneralSection").style.display = "inline-block";
  document.getElementById("updateGeneralSection").style.display = "none";
  document.getElementById("general-news").style.display = "none";
  document.getElementById("general-requests").style.display = "none";
  document.getElementById("general-announcements").style.display = "none";
  document.getElementById("editor-container-news1").style.display = "block";
  document.getElementById("editor-container-requests1").style.display = "block";
  document.getElementById("editor-container-announcements1").style.display ="block";

  getGeneralInfo();
}

//Display the general section of the web app
async function getGeneralInfo() {
  const url = server + "main/general/" + protocol_id + "/";
  let general;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    general = await res.json();
  });

  document.getElementById("editor-container-news1").innerHTML =
    general[0].general_news;
  document.getElementById("editor-container-requests1").innerHTML =
    general[0].general_requests;
  document.getElementById("editor-container-announcements1").innerHTML =
    general[0].general_announcements;
}

//Edit the general section of the web app
async function editGeneralSection() {
  document.getElementById("editGeneralSection").style.display = "none";
  quill3.enable();
  quill4.enable();
  quill5.enable();
  document.getElementById("updateGeneralSection").style.display =
    "inline-block";
  document.getElementById("cancelGeneralSection").style.display =
    "inline-block";
  document.getElementById("general-news").style.display = "block";
  document.getElementById("general-requests").style.display = "block";
  document.getElementById("general-announcements").style.display = "block";
  quill3.root.innerHTML = document.getElementById(
    "editor-container-news1"
  ).innerHTML;
  quill4.root.innerHTML = document.getElementById(
    "editor-container-requests1"
  ).innerHTML;
  quill5.root.innerHTML = document.getElementById(
    "editor-container-announcements1"
  ).innerHTML;
  document.getElementById("editor-container-news1").style.display = "none";
  document.getElementById("editor-container-requests1").style.display = "none";
  document.getElementById("editor-container-announcements1").style.display =
    "none";
}

//Get the initial content that is being displayed once the page is reloaded
async function getInitialContent() {
  quill3.disable();
  quill4.disable();
  quill5.disable();
  document.getElementById("updateGeneralSection").style.display = "none";
  document.getElementById("editGeneralSection").style.display = "none";
  document.getElementById("informationSection").style.display = "none";
  document.getElementById("participantss").style.display = "none";
  document.getElementById("absentt").style.display = "none";
  document.getElementById("moderatorr").style.display = "none";
  document.getElementById("recorderr").style.display = "none";
  document.getElementById("protocol_date").style.display = "none";
  document.getElementById("location").style.display = "none";
  document.getElementById("start_time").style.display = "none";
  document.getElementById("end_time").style.display = "none";
  document.getElementById("informationSectionEdit").style.display = "none";
  document.getElementById("general-news").style.display = "none";
  document.getElementById("general-requests").style.display = "none";
  document.getElementById("general-announcements").style.display = "none";
  document.getElementById("team1").style.display = "none";
  document.getElementById("team2").style.display = "none";
  document.getElementById("editor-container-team2-2").style.display = "block";
  document.getElementById("editor-container-team1-1").style.display = "block";
  document.getElementById("add_updateTeamGoals").style.display = "none";
  document.getElementById("cancelTeamGoals").style.display = "none";
  document.getElementById("individual").style.display = "none";
  document.getElementById("saveIndividualGoals").style.display = "none";
  document.getElementById("cancel1IndividualGoals").style.display = "none";
  document.getElementById("updateIndividualGoals").style.display = "none";
  document.getElementById("cancelIndividualGoals").style.display = "none";
  //document.getElementById("cancelInformationSection").style.display = "none";
  document.getElementById("cancelGeneralSection").style.display = "none";
}

//Display different functionalities for a recorder and employee
async function checkStatus() {
  var val = document.getElementById("status").value;

  if (val == "one") {
    quill3.disable();
    quill4.disable();
    quill5.disable();
    document.getElementById("updateGeneralSection").style.display = "none";
    document.getElementById("updateGeneralSection").style.display = "none";
    document.getElementById("editGeneralSection").style.display = "none";
    document.getElementById("informationSection").style.display = "none";
    document.getElementById("participantss").style.display = "none";
    document.getElementById("absentt").style.display = "none";
    document.getElementById("moderatorr").style.display = "none";
    document.getElementById("recorderr").style.display = "none";
    document.getElementById("protocol_date").style.display = "none";
    document.getElementById("location").style.display = "none";
    document.getElementById("start_time").style.display = "none";
    document.getElementById("end_time").style.display = "none";
    document.getElementById("informationSectionEdit").style.display = "none";
    document.getElementById("displayLocation").style.display = "block";
    document.getElementById("displayStartTime").style.display = "block";
    document.getElementById("displayEndTime").style.display = "block";
    document.getElementById("displayDate").style.display = "block";
    document.getElementById("displayModerator").style.display = "block";
    document.getElementById("displayRecorder").style.display = "block";
    document.getElementById("displayParticipants").style.display = "block";
    document.getElementById("displayAbsentees").style.display = "block";
    document.getElementById("general-news").style.display = "none";
    document.getElementById("general-requests").style.display = "none";
    document.getElementById("general-announcements").style.display = "none";
    document.getElementById("editor-container-news1").style.display = "block";
    document.getElementById("editor-container-requests1").style.display =
      "block";
    document.getElementById("editor-container-announcements1").style.display =
      "block";
    document.getElementById("team1").style.display = "block";
    document.getElementById("team2").style.display = "block";
    document.getElementById("editor-container-team2-2").style.display = "none";
    document.getElementById("editor-container-team1-1").style.display = "none";
  } else if (val == "two") {
    quill3.disable();
    quill4.disable();
    quill5.disable();
    document.getElementById("updateGeneralSection").style.display = "none";
    document.getElementById("editGeneralSection").style.display =
      "inline-block";
    document.getElementById("informationSection").style.display =
      "inline-block";
    document.getElementById("informationSectionEdit").style.display =
      "inline-block";
    document.getElementById("informationSection").style.display = "none";
    document.getElementById("editTeamGoals").style.display = "inline-block";
  }
}

//Change display when editing the information section
async function editInformationSection() {
  document.getElementById("informationSection").style.display = "inline-block";
  //document.getElementById("cancelInformationSection").style.display = "inline-block";
  document.getElementById("informationSectionEdit").style.display = "none";
  document.getElementById("participantss").style.display = "block";
  document.getElementById("absentt").style.display = "block";
  document.getElementById("moderatorr").style.display = "block";
  document.getElementById("recorderr").style.display = "block";
  document.getElementById("protocol_date").style.display = "block";
  document.getElementById("location").style.display = "block";
  document.getElementById("start_time").style.display = "block";
  document.getElementById("end_time").style.display = "block";
  document.getElementById("displayLocation").style.display = "none";
  document.getElementById("displayStartTime").style.display = "none";
  document.getElementById("displayEndTime").style.display = "none";
  document.getElementById("displayDate").style.display = "none";
  document.getElementById("displayModerator").style.display = "none";
  document.getElementById("displayRecorder").style.display = "none";
  document.getElementById("displayParticipants").style.display = "none";
  document.getElementById("displayAbsentees").style.display = "none";
  

  deleteParticipants();
  deleteAbsentees();
}

//Get team id of the user
async function getTeam() {
  let url = server + "main/" + "team/" + userid ;
  let response;

  var request = new Request(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
  });
  await fetch(request).then(async (res) => {
    response = await res.json();
  });

  team_id = response[0].team_id;
}

//Add or update team goals
async function add_updateTeamGoals() {
  getTeam();

  team_one = document.getElementById("editor-container-team1-1").innerHTML;

  team_two = document.getElementById("editor-container-team2-2").innerHTML;

  if (team_id == 1) {
    goal_description = quill1.root.innerHTML;
    if (team_one === "") {
      addTeamGoals();
    } else {
      updateGoals();
    }
  }
  if (team_id == 2) {
    goal_description = quill6.root.innerHTML;
    if (team_two === "") {
      addTeamGoals();
    } else {
      updateGoals();
    }
  }

  document.getElementById("editTeamGoals").style.display = "inline-block";
  document.getElementById("add_updateTeamGoals").style.display = "none";
  document.getElementById("cancelTeamGoals").style.display = "none";
  document.getElementById("team1").style.display = "none";
  document.getElementById("team2").style.display = "none";
  document.getElementById("editor-container-team1-1").style.display = "block";
  document.getElementById("editor-container-team2-2").style.display = "block";
}

//Add team goals in the web app
async function addTeamGoals() {
  const url = server + "main/" + "goal/team" + "/";
  let data = {
    goal_description: goal_description,
    userid: userid,
    team_id: team_id,
    protocol_id: protocol_id,
  };
  var request = new Request(url, {
    method: "PUT",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (response) => {
    await response.text();
  });

  getTeamGoal();
  team_one = document.getElementById("editor-container-team1-1").innerHTML;
  team_two = document.getElementById("editor-container-team2-2").innerHTML;
}

//Update existent goals in the web app
async function updateGoals() {
  const url = server + "main/" + "goal" + "/" + goal_id;

  let data = {
    goal_id: goal_id,
    goal_description: goal_description,
  };
  var request = new Request(url, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });

  getTeamGoal();
  team_one = document.getElementById("editor-container-team1-1").innerHTML;

  team_two = document.getElementById("editor-container-team2-2").innerHTML;
}

//Display team goals
async function getTeamGoal() {
  getTeam();

  document.getElementById("editor-container-team1-1").innerHTML = "";
  document.getElementById("editor-container-team2-2").innerHTML = "";

  const url = server + "main/goal/team" + "/" + protocol_id + "/";
  let goal;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  await fetch(request).then(async (res) => {
    goal = await res.json();
  });

  if (goal[0].team_id == 1) {
    document.getElementById("editor-container-team1-1").innerHTML =
      goal[0].goal_description;
    goal_id = goal[0].goal_id;
  } else if (goal[0].team_id == 2) {
    document.getElementById("editor-container-team2-2").innerHTML =
      goal[0].goal_description;
    goal_id = goal[0].goal_id;
  }

  if (goal[1].team_id == 1) {
    document.getElementById("editor-container-team1-1").innerHTML =
      goal[1].goal_description;
    goal_id = goal[1].goal_id;
  } else if (goal[1].team_id == 2) {
    document.getElementById("editor-container-team2-2").innerHTML =
      goal[1].goal_description;
    goal_id = goal[1].goal_id;
  }
}

//Change display when editing team goals
async function editTeamGoals() {
  getTeam();
  document.getElementById("add_updateTeamGoals").style.display = "inline-block";
  document.getElementById("cancelTeamGoals").style.display = "inline-block";

  if (team_id == 1) {
    document.getElementById("team1").style.display = "block";
    document.getElementById("editor-container-team1-1").style.display = "none";
    quill1.root.innerHTML = document.getElementById(
      "editor-container-team1-1"
    ).innerHTML;
  } else if (team_id == 2) {
    document.getElementById("team2").style.display = "block";
    document.getElementById("editor-container-team2-2").style.display = "none";
    quill6.root.innerHTML = document.getElementById(
      "editor-container-team2-2"
    ).innerHTML;
  }
}

//Cancel changes
async function cancelTeamGoals() {

  document.getElementById("team1").style.display = "none";
  document.getElementById("team2").style.display = "none";
  document.getElementById("cancelTeamGoals").style.display = "none";
  document.getElementById("add_updateTeamGoals").style.display = "none";
  document.getElementById("editor-container-team1-1").style.display = "block";
  document.getElementById("editor-container-team2-2").style.display = "block";

  
}

async function cancelGeneralSection() {
  document.getElementById("editGeneralSection").style.display = "inline-block";
  document.getElementById("updateGeneralSection").style.display = "none";
  document.getElementById("cancelGeneralSection").style.display = "none";
  document.getElementById("general-news").style.display = "none";
  document.getElementById("general-requests").style.display = "none";
  document.getElementById("general-announcements").style.display = "none";
  document.getElementById("editor-container-news1").style.display = "block";
  document.getElementById("editor-container-requests1").style.display = "block";
  document.getElementById("editor-container-announcements1").style.display ="block";
}

//Change display when adding individual goals
async function addIndividualGoals() {
  document.getElementById("individual").style.display = "block";
  document.getElementById("saveIndividualGoals").style.display = "inline-block";
  document.getElementById("cancel1IndividualGoals").style.display = "inline-block";
  document.getElementById("addIndividualGoals").style.display = "none";
  document.getElementById("cancelIndividualGoals").style.display = "none";
  document.getElementById("updateIndividualGoals").style.display = "none";
}

//Save a new individual goal
async function saveIndividualGoals() {
  goal_description = quill2.root.innerHTML;

  const url = server + "main/" + "goal/individual" + "/";
  let data = {
    goal_description: goal_description,
    userid: userid,
    team_id: 4,
    protocol_id: protocol_id,
  };
  var request = new Request(url, {
    method: "PUT",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (response) => {
    await response.text();
  });

  document.getElementById("individual").style.display = "none";
  document.getElementById("saveIndividualGoals").style.display = "none";
  document.getElementById("cancel1IndividualGoals").style.display = "none";
  document.getElementById("addIndividualGoals").style.display = "inline-block";

  getIndividualGoal();
}

//Cancel an individual goal
async function cancel1IndividualGoals() {
  
  document.getElementById("individual").style.display = "none";
  document.getElementById("saveIndividualGoals").style.display = "none";
  document.getElementById("cancel1IndividualGoals").style.display = "none";
  document.getElementById("addIndividualGoals").style.display = "inline-block";

}

//Display each new individual goals
async function getIndividualGoal() {
  const url =
    server + "main/goal/individual" + "/" + protocol_id + "/";

  let goal;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  await fetch(request).then(async (res) => {
    goal = await res.json();
  });

  var size = Object.keys(goal).length;

  individualGoals = document.getElementById("individual_goals");

  newHeader = document.createElement("h6");
  newHeader.innerHTML = goal[size - 1].prename + " " + goal[size - 1].name;
  newHeader.setAttribute("id", "header" + goal[size - 1].goal_id);

  newDiv = document.createElement("div");
  newDiv.innerHTML = goal[size - 1].goal_description;
  newDiv.setAttribute("id", goal[size - 1].goal_id);
  individualGoals.appendChild(newHeader);
  individualGoals.appendChild(newDiv);

  buttons = document.createElement("div");
  buttonPencil = document.createElement("button");
  iconPencil = document.createElement("i");
  iconPencil.innerHTML = '<i class="fa-solid fa-pen" aria-hidden="true"></i>';
  buttonPencil.setAttribute("disabled", "disabled");
  if (userid == goal[size - 1].userid) {
    buttonPencil.removeAttribute("disabled");
  }
  buttonPencil.appendChild(iconPencil);
  buttons.appendChild(buttonPencil);
  buttonPencil.setAttribute(
    "onclick",
    "editIndividualGoal(this.parentElement.parentElement)"
  );

  buttonTrash = document.createElement("button");
  iconTrash = document.createElement("i");
  iconTrash.innerHTML = '<i class="fa-solid fa-trash" aria-hidden="true"></i>';
  buttonTrash.setAttribute("disabled", "disabled");
  if (userid == goal[size - 1].userid) {
    buttonTrash.removeAttribute("disabled");
  }

  buttonTrash.setAttribute(
    "onclick",
    "deleteIndividualGoal(this.parentElement.parentElement)"
  );

  buttonTrash.appendChild(iconTrash);
  buttons.appendChild(buttonTrash);
  newDiv.appendChild(buttons);
  buttons.style.float = "right";
  buttons.style.marginTop = "0px";
  buttons.style.marginBottom = "20px";
  document.getElementById(goal[size - 1].goal_id).style.minHeight = "80px";
  document.getElementById(goal[size - 1].goal_id).style.boxSizing =
    "border-box";
  document.getElementById(goal[size - 1].goal_id).style.height = "150%";
  document.getElementById(goal[size - 1].goal_id).style.width = "100%";
  document.getElementById(goal[size - 1].goal_id).style.border =
    "2px solid #CCCCCC";
  document.getElementById(goal[size - 1].goal_id).style.borderRadius = "10px";
  document.getElementById(goal[size - 1].goal_id).style.marginRight = "15px";
  document.getElementById(goal[size - 1].goal_id).style.paddingLeft = "15px";
  document.getElementById(goal[size - 1].goal_id).style.paddingTop = "5px";
  document.getElementById(goal[size - 1].goal_id).style.marginTop = "7px";
  document.getElementById(goal[size - 1].goal_id).style.paddingBottom = "25px";
  document.getElementById(goal[size - 1].goal_id).style.marginBottom = "25px";
}

//Display the list of individual goals together
async function getInitialIndividualGoals() {
  document.getElementById("individual_goals").innerHTML = "";

  const url =
  server + "main/goal/individual" + "/" + protocol_id + "/";
  let goal;

  var request = new Request(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  await fetch(request).then(async (res) => {
    goal = await res.json();
  });

  individualGoals = document.getElementById("individual_goals");

  var size = Object.keys(goal).length;

  goal.forEach((element) => {
    newHeader = document.createElement("h6");
    newHeader.innerHTML = element.prename + " " + element.name;
    newHeader.setAttribute("id", "header" + element.goal_id);
    individualGoals.appendChild(newHeader);
    newDiv = document.createElement("div");
    newDiv.innerHTML = element.goal_description;
    newDiv.setAttribute("id", element.goal_id);
    individualGoals.appendChild(newDiv);
    buttons = document.createElement("div");
    buttonPencil = document.createElement("button");
    iconPencil = document.createElement("i");
    iconPencil.innerHTML = '<i class="fa-solid fa-pen" aria-hidden="true"></i>';
    buttonPencil.setAttribute("disabled", "disabled");
    if (userid == element.userid) {
      buttonPencil.removeAttribute("disabled");
    }
    buttonPencil.appendChild(iconPencil);
    buttons.appendChild(buttonPencil);
    buttonPencil.setAttribute(
      "onclick",
      "editIndividualGoal(this.parentElement.parentElement)"
    );

    buttonTrash = document.createElement("button");
    iconTrash = document.createElement("i");
    iconTrash.innerHTML =
      '<i class="fa-solid fa-trash" aria-hidden="true"></i>';
    buttonTrash.setAttribute("disabled", "disabled");
    if (userid == element.userid) {
      buttonTrash.removeAttribute("disabled");
    }

    buttonTrash.setAttribute(
      "onclick",
      "deleteIndividualGoal(this.parentElement.parentElement)"
    );

    buttonTrash.appendChild(iconTrash);
    buttons.appendChild(buttonTrash);
    newDiv.appendChild(buttons);
    buttons.style.float = "right";
    buttons.style.marginTop = "0px";
    buttons.style.marginBottom = "20px";
  });

  for (j = 0; j < size; j++) {
    document.getElementById(goal[j].goal_id).style.minHeight = "80px";
    document.getElementById(goal[j].goal_id).style.boxSizing = "border-box";
    document.getElementById(goal[j].goal_id).style.height = "150%";
    document.getElementById(goal[j].goal_id).style.width = "100%";
    document.getElementById(goal[j].goal_id).style.border = "2px solid #CCCCCC";
    document.getElementById(goal[j].goal_id).style.borderRadius = "10px";
    document.getElementById(goal[j].goal_id).style.marginRight = "15px";
    document.getElementById(goal[j].goal_id).style.paddingLeft = "15px";
    document.getElementById(goal[j].goal_id).style.paddingTop = "5px";
    document.getElementById(goal[j].goal_id).style.marginTop = "7px";
    document.getElementById(goal[j].goal_id).style.paddingBottom = "25px";
    document.getElementById(goal[j].goal_id).style.marginBottom = "25px";
  }
}

//Delete an individual goal
async function deleteIndividualGoal(elem) {
  const goal_id = elem.getAttribute("id");

  const url = server + "main/" + "delete" + "/";
  let data = {
    goal_id: goal_id,
  };
  var request = new Request(url, {
    method: "PUT",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });

  var delete_goal = document.getElementById(goal_id);
  var delete_name = document.getElementById("header" + goal_id);
  delete_goal.parentNode.removeChild(delete_goal);
  delete_name.parentNode.removeChild(delete_name);
}

async function editIndividualGoal(elem) {
  const elemId = elem.getAttribute("id");
  goal = document.getElementById(elemId);
  update = document.getElementById("updateIndividualGoals");
  cancel = document.getElementById("cancelIndividualGoals");

  individual = document.getElementById("individual");
  document.getElementById("individual").style.display = "block";
  document.getElementById("updateIndividualGoals").style.display =
    "inline-block";
  document.getElementById("cancelIndividualGoals").style.display =
    "inline-block";
  quill2.root.innerHTML = document.getElementById(elemId).innerHTML;
  document.getElementById(elemId).style.display = "none";
  document.getElementById("header" + elemId).style.display = "none";

  update.setAttribute("onclick", "updateIndividualGoals(goal)");

  cancel.setAttribute("onclick", "cancelIndividualGoals(goal)");
}

//Change display of the section when cancel button is clicked
async function cancelIndividualGoals(goal) {
  goal_id = goal.getAttribute("id");
  document.getElementById("individual").style.display = "none";
  document.getElementById("updateIndividualGoals").style.display = "none";
  document.getElementById("cancelIndividualGoals").style.display = "none";
  document.getElementById(goal_id).style.display = "block";
  document.getElementById("header" + goal_id).style.display = "block";
  document.getElementById("saveIndividualGoals").style.display = "none";
  document.getElementById("cancel1IndividualGoals").style.display = "none";
  document.getElementById("addIndividualGoals").style.display = "inline-block";
}

//Update individual goals
async function updateIndividualGoals(goal) {
  goal_id = goal.getAttribute("id");
  goal_description = quill2.root.innerHTML;
  const url = server + "main/" + "goal" + "/" + goal_id;

  let data = {
    goal_id: goal_id,
    goal_description: goal_description,
  };
  var request = new Request(url, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });

  document.getElementById("individual").style.display = "none";
  document.getElementById("updateIndividualGoals").style.display = "none";
  document.getElementById("saveIndividualGoals").style.display = "none";
  document.getElementById("cancel1IndividualGoals").style.display = "none";
  document.getElementById("cancelIndividualGoals").style.display = "none";
  document.getElementById("header" + goal_id).style.display = "block";
  goal = document.getElementById(goal_id);
  document.getElementById(goal_id).style.display = "block";
  document.getElementById(goal_id).innerHTML = quill2.root.innerHTML;

  buttons = document.createElement("div");
  buttonPencil = document.createElement("button");
  iconPencil = document.createElement("i");
  iconPencil.innerHTML = '<i class="fa-solid fa-pen" aria-hidden="true"></i>';
  buttonPencil.appendChild(iconPencil);
  buttons.appendChild(buttonPencil);
  buttonPencil.setAttribute(
    "onclick",
    "editIndividualGoal(this.parentElement.parentElement)"
  );

  buttonTrash = document.createElement("button");
  iconTrash = document.createElement("i");
  iconTrash.innerHTML = '<i class="fa-solid fa-trash" aria-hidden="true"></i>';
  buttonTrash.appendChild(iconTrash);
  buttons.appendChild(buttonTrash);
  goal.appendChild(buttons);

  buttons.style.float = "right";
  buttons.style.marginTop = "0px";
  buttons.style.marginBottom = "20px";
}

//Sign out from the web
async function signout() {
  window.location.replace("https://protocolarchive.azurewebsites.net");
}

function download() {
  var opt = {
    margin:       [15, 15, 15, 15],
  }
  
  const element = document.getElementById('wholeContent');
  // Choose the element and save the PDF for our user.
  html2pdf().from(element).save();
}