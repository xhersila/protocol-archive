
var toolbarOptions = [
    [{ 'size': ['small', false, 'large', 'huge'] }], 
    [{ 'header': 1 }, { 'header': 2 }],  
    ['bold', 'italic', 'underline', 'strike'], 
  ['blockquote', 'code-block'],
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],      
  [{ 'indent': '-1'}, { 'indent': '+1' }],          
  [{ 'direction': 'rtl' }],                       
  [{ 'color': [] }, { 'background': [] }],         
  [{ 'font': [] }],
  [{ 'align': [] }],  
    ];

  //Implement rich text editors with the above toolbar options
     var quill1 = new Quill('#editor-container-team1', {
      modules: {
          toolbar: toolbarOptions
      },
      placeholder: 'Write Your Team Goals...',
      theme: 'snow'
  });
   var quill2 = new Quill('#editor-container-individual', {
      modules: {
          toolbar: toolbarOptions
      },
      placeholder: 'Write Your Indivudal Goals...',
      theme: 'snow' 
  });
  
   var quill3 = new Quill('#editor-container-general', {
      modules: {
          toolbar: toolbarOptions
      },
      placeholder: '...',
      theme: 'snow'
  });
  
   var quill4 = new Quill('#editor-container-requests', {
      modules: {
          toolbar: toolbarOptions
      },
      placeholder: '...',
      theme: 'snow' // or 'bubble'
  });

  
   var quill5 = new Quill('#editor-container-announcements', {
      modules: {
          toolbar: toolbarOptions
      },
      placeholder: '...',
      theme: 'snow' // or 'bubble'
  });
  
  

   var quill6 = new Quill('#editor-container-team2', {
      modules: {
          toolbar: toolbarOptions
      },
      placeholder: 'Write Your Team Goals...',
      theme: 'snow' // or 'bubble'
  });

