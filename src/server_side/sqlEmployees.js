const pool = require("./connectionPool");

//Add participants in the database
async function addParticipants(userid, protocol_id, participant_name) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `INSERT INTO protocol_participant(userid,protocol_id, participant_name) values ('${userid}','${protocol_id}', '${participant_name}');`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Delete participants from the database
async function deleteParticipants(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `DELETE FROM protocol_participant WHERE protocol_id = '${protocol_id}';`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Add absentees in the database
async function addAbsentees(userid, protocol_id, absentee_name) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `INSERT INTO protocol_absentee(userid,protocol_id, absentee_name) values ('${userid}','${protocol_id}', '${absentee_name}');`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Delete absentees from the database
async function deleteAbsentees(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `DELETE FROM protocol_absentee WHERE protocol_id = '${protocol_id}';`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Display participants
async function getParticipants(protocol_id) {
  let conn;
  let res;

  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT DISTINCT participant_name FROM protocol_participant where protocol_id = '${protocol_id}';`
    );

    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Display Absentees
async function getAbsentees(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT DISTINCT absentee_name FROM protocol_absentee where protocol_id = '${protocol_id}';`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Get moderator of the week
async function getModerator(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT moderator_id FROM protocol WHERE protocol_id = '${protocol_id}';`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Get recorder of the week
async function getRecorder(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT recorder_id FROM protocol WHERE protocol_id = '${protocol_id}';`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

module.exports = {
    addParticipants,
    getParticipants,
    deleteParticipants,
    deleteAbsentees,
    addAbsentees,
    getAbsentees,
    getModerator,
    getRecorder
  };
