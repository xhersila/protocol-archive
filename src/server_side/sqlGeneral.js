const pool = require("./connectionPool");

//Get user's username
async function getUsername(userid, password) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT userid, password from user where userid = '${userid}' AND password = '${password}'`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Get user's information
async function getUserinfo(userid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT prename, name from user where userid = '${userid}'`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Get user's team id
async function getTeam(userid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT team_id from user_team where userid = '${userid}'`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

module.exports = {
  getUsername,
  getUserinfo,
  getTeam
};
