const pool = require("./connectionPool");

//Add new protocol in the database
async function addProtocol(
  protocol_start,
  protocol_end,
  protocol_id,
  userid,
  goal_id
) {
  let conn;
  let res;
  let res1;
  let res2;
  let res3;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `INSERT INTO protocol(protocol_id, protocol_start, protocol_end) VALUES ('${protocol_id}','${protocol_start}','${protocol_end}');`
    );
    res1 = await conn.query(
      `INSERT INTO protocol_goals(goal_id, protocol_id) values('${goal_id}', '${protocol_id}');`
    );
    res2 = await conn.query(
      `INSERT INTO protocol_participant(userid,protocol_id) values('${userid}', '${protocol_id}');`
    );
    res3 = await conn.query(
      `INSERT INTO protocol_ absentee(userid, protocol_id) values ('${userid}', '${protocol_id}');`
    );
  } catch (err) {
    if (conn) {
      conn.end();
    }
    return res + res1 + res2 + res3;
  }
}

//Edit information section of the protocol
async function editInformationSection(
  protocol_id,
  date,
  location,
  start_time,
  end_time,
  recorder_id,
  moderator_id
) {
  let conn;
  let res;

  try {
    conn = await pool.getConnection();
    res =
      await conn.query(`UPDATE protocol SET date = '${date}', location = '${location}',
      start_time = '${start_time}', end_time = '${end_time}', recorder_id = '${recorder_id}',
      moderator_id = '${moderator_id}' WHERE protocol_id = '${protocol_id}';`);
  } catch (err) {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Edit general section of the protocol
async function editGeneralSection(
  protocol_id,
  general_news,
  general_requests,
  general_announcements
) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res =
      await conn.query(`UPDATE protocol SET general_news = '${general_news}', general_requests = '${general_requests}',
      general_announcements = '${general_announcements}'
      where protocol_id = '${protocol_id}';`);
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Display general information section of the protocol
async function getGeneralInformation(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT general_news,general_requests, general_announcements FROM protocol where protocol_id = '${protocol_id}';`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Display information section of the protocol
async function getInformationSection(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `SELECT location, start_time, end_time, date, recorder_id, moderator_id FROM protocol where protocol_id = '${protocol_id}';`
    );
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

module.exports = {
    addProtocol,
    editInformationSection,
    editGeneralSection,
    getGeneralInformation,
    getInformationSection,
  };