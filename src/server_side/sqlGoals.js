const pool = require("./connectionPool");

//Add team goals in the database
async function addTeamGoal(goal_description, userid, team_id, protocol_id) {
  let conn;
  let res;
  let res2;
  let res3;
  let res4;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `INSERT INTO goals(goal_description,goal_type) values ('${goal_description}', 'team');`
    );
    const goal_id = res.insertId;
    res3 = await conn.query(
      `INSERT INTO protocol_goals(goal_id,protocol_id) values(${goal_id}, '${protocol_id}');`
    );
    res4 = await conn.query(
      `INSERT INTO goals_team(goal_id, team_id) values(${goal_id}, ${team_id});`
    );
    res2 = await conn.query(
      `INSERT INTO user_goals(goal_id, userid) values(${goal_id}, '${userid}');`
    );
    delete res.meta;
    delete res2.meta;
    delete res3.meta;
    delete res4.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res + res2 + res3 + res4;
  }
}

//Display team goals
async function getTeamGoal(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res =
      await conn.query(`SELECT distinct goals.goal_description, goals_team.team_id, goals.goal_id FROM goals, goals_team, protocol_goals
    WHERE goals.goal_id = goals_team.goal_id 
    and goals.goal_id = protocol_goals.goal_id 
    and protocol_goals.protocol_id ='${protocol_id}'
    and goal_type = 'team';`);
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Add individual goals in the database
async function addIndividualGoal(
  goal_description,
  userid,
  team_id,
  protocol_id
) {
  let conn;
  let res;
  let res2;
  let res3;
  let res4;
  try {
    conn = await pool.getConnection();
    res = await conn.query(
      `INSERT INTO goals(goal_description,goal_type) values ("${goal_description}", 'individual');`
    );
    const goal_id = res.insertId;
    res3 = await conn.query(
      `INSERT INTO protocol_goals(goal_id,protocol_id) values(${goal_id}, '${protocol_id}');`
    );
    res4 = await conn.query(
      `INSERT INTO goals_team(goal_id, team_id) values(${goal_id}, ${team_id});`
    );
    res2 = await conn.query(
      `INSERT INTO user_goals(goal_id, userid) values(${goal_id}, '${userid}');`
    );
    delete res.meta;
    delete res2.meta;
    delete res3.meta;
    delete res4.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res + res2 + res3 + res4;
  }
}

//Display individual goals
async function getIndividualGoal(protocol_id) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res =
      await conn.query(`SELECT distinct user.prename, user.name, user_goals.userid, goals.goal_description, goals.goal_id FROM goals, goals_team, protocol_goals, user_goals, user
    WHERE goals.goal_id = goals_team.goal_id 
    and goals.goal_id = protocol_goals.goal_id
    and goals.goal_id = user_goals.goal_id 
    and user_goals.userid = user.userid
    and protocol_goals.protocol_id ='${protocol_id}'
    and goal_type = 'individual';`);
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Edit existent goals
async function editGoal(goal_id, goal_description) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res =
      await conn.query(`UPDATE goals SET goal_description = '${goal_description}'
    where goal_id = ${goal_id};`);
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

//Delete existent goals
async function deleteGoal(goal_id) {
  let conn;
  let res, res2, res3, res4;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`DELETE FROM goals_team WHERE goal_id = ${goal_id}`);
    res2 = await conn.query(
      `DELETE FROM protocol_goals WHERE goal_id = ${goal_id}`
    );
    res3 = await conn.query(
      `DELETE FROM user_goals WHERE goal_id = ${goal_id}`
    );
    res4 = await conn.query(`DELETE FROM goals WHERE goal_id = ${goal_id}`);
    delete res.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end();
    }
    return res;
  }
}

module.exports = {
    addTeamGoal,
    editGoal,
    getTeamGoal,
    addIndividualGoal,
    getIndividualGoal,
    deleteGoal,
  };
