const mariadb = require("mariadb");

const pool = mariadb.createPool({
  host: "raevoluzwebappdb.mariadb.database.azure.com",
  user: "raevoluz_admin@raevoluzwebappdb",
  password: "test_Account",
  database: "einkaufsplaner",
  ssl: true,
  connectionLimit: 5,
});

module.exports = pool;
