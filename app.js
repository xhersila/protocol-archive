var express = require("express");
var path = require("path");

const app = express();
app.use(express.json());

constGeneralRoute = require('./src/routes/general');
constEmployeesRoute = require('./src/routes/employees');
constGoalRoute = require('./src/routes/goal');
constProtocolRoute = require('./src/routes/protocol');

app.use('/', constGeneralRoute);
app.use('/', constEmployeesRoute);
app.use('/', constGoalRoute);
app.use('/', constProtocolRoute);

app.set("views", path.join("./src/client_side", "views"));
app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");

app.use(express.static("src/client_side/stylesheets"));
app.use(express.static("src/client_side/js"));

app.get('/', function(req, res, next) {
    res.render('login');
});

app.get("/main/:userid/", function (req, res, next) {
    res.render("index");
  });



module.exports = app;
